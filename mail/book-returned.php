<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<h2><?= Yii::t('app', 'Thanks you for your returning... '); ?></h2>
<h3><?= Yii::t('app', 'Your BubbleBook material it\'s back in our Library.') ?></h3>
<p>
    <?= Yii::t('app', 'We, hope you have been enjoyed your reading!') ?>
    <?= Yii::t('app', 'Please, don\'t hesitate on pursuit the knowledge.') ?>
</p>
<p style="text-align: center">
    <?= Html::a(Yii::t('app', 'Visit Our Library'), Url::home('http')) ?>
</p>
<p><?= Yii::t('app', 'Best Regards, BubbleUp Librarian'); ?></p>
