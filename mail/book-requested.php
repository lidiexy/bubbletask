<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<h2><?= Yii::t('app', 'Reading soon...'); ?></h2>
<h3><?= Yii::t('app', 'Your BubbleBook material it\'s ready for pickup') ?></h3>
<p>
    <?= Yii::t('app', 'Please, come withing 3 days.') ?>
    <?= Yii::t('app', 'Remember you have 7 days to return this material back to be used for others readers.') ?>
</p>
<p style="text-align: center">
    <?= Html::a(Yii::t('app', 'Visit Our Library'), Url::home('http')) ?>
</p>
<p><?= Yii::t('app', 'Best Regards, BubbleUp Librarian'); ?></p>

