<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<h2><?= Yii::t('app', 'We need the BubbleBook\'s material back ASAP'); ?></h2>
<h3><?= Yii::t('app', 'Your BubbleBook material past the devolution date.') ?></h3>
<p>
    <?= Yii::t('app', 'We, hope you have been enjoyed your reading!') ?>
    <?= Yii::t('app', 'But, more readers are requesting this material.') ?>
</p>
<p style="text-align: center">
    <?= Html::a(Yii::t('app', 'Read Our Policies'), Url::toRoute(['/site/circulation-policies'])) ?>
</p>
<p><?= Yii::t('app', 'Best Regards, BubbleUp Librarian'); ?></p>

