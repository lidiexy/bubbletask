Yii 2 BubbleUp Task
============================

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application.

Using the [yii2-user](https://github.com/amnah/yii2-user) extensions for basic User Management.

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-basic/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-basic/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-basic.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-basic)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------
### Need if not installed
~~~
php composer.phar global require "fxp/composer-asset-plugin:^1.3.1"
~~~

### Install via Composer

Please, download or clone the Bitbucket repo. Public in [bubbletask](https://bitbucket.org/lidiexy/bubbletask)
Then, run the commands to update the packages.

~~~
composer update
~~~

Now you should be able to access the application through the following URL, assuming `bubbleup` is the directory
directly under the Web root.

~~~
http://localhost/bubbleup/web/
~~~

All the implementation was made within a Vagrant Environment.


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=bubbleteask',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

### Migrations ###

All the tables are migrations, please execute:
~~~
php yii migrate
~~~

To get the basic data structure and login with the credentials 

### Data Generation ###
After login as Administrator (Librarian) there is a **menu option** to _generate
10 books_ at a time.

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.


TESTING
-------

Tests are located in `tests` directory. They are developed with [Codeception PHP Testing Framework](http://codeception.com/).
By default there are 3 test suites:

- `unit`
- `functional`
- `acceptance`

Tests can be executed by running

```
vendor/bin/codecept run
``` 

The command above will execute unit and functional tests. Unit tests are testing the system components, while functional
tests are for testing user interaction. Acceptance tests are disabled by default as they require additional setup since
they perform testing in real browser. 

**NOTES ABOUT TEST OR TDD:**

The Tests are barely started. Please don't executed.

