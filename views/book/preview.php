<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use Carbon\Carbon;
use kartik\icons\Icon;
Icon::map($this);

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-preview">
    <div class="row">
        <div class="col-xs-12">

            <?php if (Yii::$app->session->hasFlash('errorBorrowing')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <?= Yii::t('app', 'Missing Pages :(. Sorry, your request wasn\'t complete. Try later.'); ?>
                </div>
            <?php endif; ?>

            <div class="panel">
                <div class="panel-body">
                    <h4 class="text-semibold no-margin-top">
                        <?= Html::encode($model->title); ?>
                        <span class="badge badge-info"><?=  $model->existence ?> copies</span>
                        <?= ($model->existence <= 0) ? '<span class="label label-danger">Out of Inventory</span>' : ''; ?>
                    </h4>
                    <h6 class="text-success pull-right no-margin text-semibold"><small class="text-muted text-uppercase">price</small> $<?= money_format('%.2n', $model->price); ?></h6>
                    <div class="book-author">
                        <label class="text-uppercase text-semibold small">Author</label>
                        <?= Icon::show('angle-right') ?>
                        <span class="text-semibold text-danger"> <?= Html::encode($model->author); ?> </span>
                    </div>

                    <p class="semi-lead"><?= Html::encode($model->review); ?></p>

                    <p class="well">
                        Borrowing a copy of this material is under your agreement of our <strong>borrowing rules</strong> and the
                        <?= Html::a(Yii::t('app', 'Circulation Policies'), '/site/circulation-policies'); ?>.
                        Remember you have <strong> 7 days to return this material</strong>. Any questions? Please contact us.
                    </p>

                    <div class="terms-and-action text-center">
                        <?php
                        if($model->existence > 0) {
                            echo Html::a(Yii::t('app', 'Borrow a Copy') . ' &nbsp; ' . Icon::show('angle-right') , ['borrow', 'id' => $model->id], [
                                'class' => 'btn btn-success',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you agreed with our return policy?'),
                                    'method' => 'post',
                                ],
                            ]);
                        } else {
                            echo Html::a(Yii::t('app', 'Sorry, Not Available') . ' &nbsp; ' . Icon::show('ban') , ['#'], [
                                'class' => 'btn btn-default disabled'
                            ]);
                        }
                        ?>
                    </div>
                </div>

                <div class="panel-footer panel-footer-condensed">
                    <div class="heading-elements not-collapsible">
                        <span class="heading-text">
                            <small class="text-muted">Published on </small>
                            <?= Carbon::createFromFormat('Y-m-d', $model->publication_date)->toFormattedDateString(); ?>
                        </span>
                        <span class="heading-text pull-right label label-warning">
                            <?= $model->genre ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
