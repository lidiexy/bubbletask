<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Books out of inventory');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">
    <h1><?= Html::encode(Yii::t('app', 'Books out of inventory. (Existense = 0)')) ?></h1>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'isbn',
            'title',
            'genre',
            'author',
            [
                'attribute' => 'total_inventory',
                'headerOptions' => ['style' => 'width:10%'],
            ],
            'publication_date:date',
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style' => 'width:8%'],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>

