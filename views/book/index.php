<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\icons\Icon;
Icon::map($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Books');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">
    <h1><?= Html::encode(Yii::t('app', 'Book Inventory')) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Add New Book'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($searchModel) {
            if($searchModel->existence == 0){
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            'isbn',
            'title',
            'genre',
            'author',
            [
                'attribute' => 'existence',
                'headerOptions' => ['style' => 'width:10%'],
                'format' => 'raw',
                'value' => function($dataProvider) {
                    if($dataProvider['existence'] == 0)
                        return Icon::show('ban', ['class' => 'text-danger']);
                    else
                        return $dataProvider['existence'];
                },
            ],
            [
                'attribute' => 'total_inventory',
                'headerOptions' => ['style' => 'width:10%'],
            ],
            'publication_date:date',
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style' => 'width:8%'],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
