<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'BubbleBooks',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $items = [
        ['label' => Yii::t('app','Home'), 'url' => ['/site/index']],
        ['label' => Yii::t('app','About'), 'url' => ['/site/about']],
        ['label' => Yii::t('app','Contact'), 'url' => ['/site/contact']],
    ];
    if(Yii::$app->user->isGuest) {
        $items[] = ['label' => Yii::t('app','Register'), 'url' => ['/user/register']];
        $items[] = ['label' => Yii::t('app','Login'), 'url' => ['/user/login']];
    } else {
        if(Yii::$app->user->can('admin')) {
            $items[] = ['label' => '<span class="glyphicon glyphicon-cog"></span> ' . Yii::t('app','Admin'), 'items' => [
                '<li class="dropdown-header">' . Yii::t('app','User Admin') . '</li>',
                ['label' => Yii::t('app','User List'), 'url' => ['/user/admin']],
                ['label' => Yii::t('app','Add New User'), 'url' => ['/user/admin/create']],
                '<li class="divider"></li>',
                '<li class="dropdown-header">' . Yii::t('app','Books') . '</li>',
                ['label' => Yii::t('app','Books Inventory'), 'url' => ['/book/index']],
                ['label' => Yii::t('app','Add New Book'), 'url' => ['/book/create']],
                ['label' => Yii::t('app','Out of Inventory'), 'url' => ['/book/out-of-inventory']],
                ['label' => Yii::t('app','Generate 10 Books'), 'url' => ['/site/generate']],
                '<li class="divider"></li>',
                '<li class="dropdown-header">' . Yii::t('app','Library Service') . '</li>',
                ['label' => Yii::t('app','All Book Requests'), 'url' => ['/borrowing/index']],
                ['label' => Yii::t('app','Books for Pickup'), 'url' => ['/borrowing/pickups']],
                ['label' => Yii::t('app','Books Returned'), 'url' => ['/borrowing/returned']],
                ['label' => Yii::t('app','Overdue Lends'), 'url' => ['/borrowing/overdue-lends']],
            ]];
        }
        $items[] = ['label' => '<span class="glyphicon glyphicon-user"></span> ' . Yii::t('app','Welcome') .  ', ' . Yii::$app->user->displayName, 'items' => [
            ['label' => Yii::t('app','Profile'), 'url' => ['/user/profile']],
            ['label' => Yii::t('app','Personal Library'), 'url' => ['/borrowing/my-books']],
            '<li class="divider"></li>',
            ['label' => '<span class="glyphicon glyphicon-off"></span> ' . Yii::t('app','Logout'),
                'url' => ['/user/logout'],
                'linkOptions' => ['data-method' => 'post']
            ]
        ]];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => $items
    ]);

    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">
            &copy; Library BubbleUp <?= date('Y') ?>. 
            Please, read our <?= Html::a(Yii::t('app', 'Circulation Policies'), '/site/circulation-policies'); ?>. Thanks!
        </p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
