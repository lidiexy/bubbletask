<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use Carbon\Carbon;
use kartik\icons\Icon;
Icon::map($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\BorrowingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Books for Pickup');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Books Request'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pikups-index">

    <?php if (Yii::$app->session->hasFlash('zeroExistence')): ?>
        <div class="alert alert-warning alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <?= Yii::t('app', 'Zero in Inventory?!? Seem like all are out there. Request more copies!'); ?>
        </div>
    <?php endif; ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'book_id',
                'filter' => false,
                'format' => 'raw',
                'value' => function($searchModel) {
                    return Html::a($searchModel->book->isbn, Url::toRoute(['/book/view', 'id' => $searchModel->book_id]));
                },
            ],
            [
                'attribute' => 'user_id',
                'filter' => false,
                'format' => 'raw',
                'value' => function($searchModel) {
                    return Html::a($searchModel->user->email, 'mailto:' . $searchModel->user->email);
                },
            ],
            'borrowed_date:date',
            'due_date:date',
            'returned_date:date',
            [
                'attribute' => 'status',
                'filter' => false,
                'format' => 'raw',
                'value' => function($model) {
                    return '<span class="label label-default">' . $model->getStatusArray()[$model->status] . '</span>';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{pickup}',
                'buttons' => [
                    'pickup' => function ($url, $model) {
                        $url = Url::toRoute(['pickup', 'id' => $model->id]);
                        return Html::a(' ' . Icon::show('vcard'), $url, [
                            'title' => Yii::t('app', 'Lend'),
                            'data' => [
                                'method' => 'post',
                            ]
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
