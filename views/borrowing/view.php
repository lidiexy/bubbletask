<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Borrowing */

$this->title = Yii::t('app', 'Loan by') . ' ' . ucfirst($model->user->getDisplayName()) ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Books Request'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="borrowing-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if(empty($model->borrowed_date)) : ?>
        <?= Html::a(Yii::t('app', 'Lend This Book'), ['pickup', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'method' => 'post',
            ],
        ]) ?>
        <?php endif; ?>
        <?php if($model->status > \app\models\Borrowing::STATUS_ON_HOLD) : ?>
            <?= Html::a(Yii::t('app', 'Return this Book'), ['return-book', 'id' => $model->id], [
                'class' => 'btn btn-warning',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure everything is OK?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'borrowed_date:date',
            'due_date:date',
            'returned_date:date',
            [
                'attribute' => 'book_id',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a('ID:' . $model->book_id . ' - ' . $model->book->title, Url::toRoute(['/book/view', 'id' => $model->book_id]));
                }
            ],
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return 'ID:' . $model->user_id . ' - ' . $model->user->displayName;
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    return '<span class="label label-default">' . $model->getStatusArray()[$model->status] . '</span>';
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
