<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Borrowing */

$this->title = Yii::t('app', 'Update') . ' '
    . $model->book->title . ' ' . Yii::t('app', 'Loan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Books Request'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="borrowing-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'books' => $books,
        'readers' => $readers
    ]) ?>

</div>
