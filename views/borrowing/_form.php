<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Borrowing */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="borrowing-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-borrowing-entry',
        'type' => ActiveForm::TYPE_VERTICAL
    ]); ?>

    <?= $form->field($model, 'borrowed_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Date of Reader Pickup?'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'due_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Due Date?'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'returned_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Date of Return'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'book_id')->widget(Select2::classname(), [
        'data' => $books,
        'options' => ['placeholder' => 'Select the book'],
        'pluginOptions' => [
            'allowClear' => false
        ],
    ]); ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => $readers,
        'options' => ['placeholder' => 'Select the reader'],
        'pluginOptions' => [
            'allowClear' => false
        ],
    ]); ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatusArray()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
