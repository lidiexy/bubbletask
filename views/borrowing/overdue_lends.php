<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use Carbon\Carbon;
use kartik\icons\Icon;
Icon::map($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\BorrowingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Overdue Lends - Reader\'s Email');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Books Request'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="overdues-index">

    <?php if (Yii::$app->session->hasFlash('bookOverdue') || Yii::$app->session->hasFlash('bulkOverdue')): ?>
        <div class="alert alert-warning alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <?php
            if(Yii::$app->session->hasFlash('bookOverdue')) {
                echo Yii::t('app', 'Email was successfully sent to the Reader!');
            } elseif (Yii::$app->session->hasFlash('bulkOverdue')) {
                echo Yii::t('app', 'Bulk Email was successfully sent to all the Reader!');
            }
            ?>
        </div>
    <?php endif; ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Send Bulk Email'), ['overdue-bulk'], [
                'class' => 'btn btn-warning',
                'data' => [
                    'method' => 'post',
                ]
            ]) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($searchModel) {
            if($searchModel->isOverdue()) {
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            [
                'attribute' => 'book_id',
                'filter' => false,
                'format' => 'raw',
                'value' => function($searchModel) {
                    return Html::a($searchModel->book->isbn, Url::toRoute(['/book/view', 'id' => $searchModel->book_id]));
                },
            ],
            [
                'attribute' => 'user_id',
                'filter' => false,
                'format' => 'raw',
                'value' => function($searchModel) {
                    return Html::a($searchModel->user->email, 'mailto:' . $searchModel->user->email);
                },
            ],
            'borrowed_date:date',
            'due_date:date',
            'returned_date:date',
            [
                'attribute' => 'status',
                'filter' => false,
                'format' => 'raw',
                'value' => function($model) {
                    return '<span class="label label-default">' . $model->getStatusArray()[$model->status] . '</span>';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{send}',
                'buttons' => [
                    'send' => function ($url, $model) {
                        $url = Url::toRoute(['overdue-email', 'id' => $model->id]);
                        return Html::a(' ' . Icon::show('envelope'), $url, [
                            'title' => Yii::t('app', 'Send Email'),
                            'data' => [
                                'method' => 'post',
                            ]
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>

