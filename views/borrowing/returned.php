<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use Carbon\Carbon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BorrowingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Books Returned');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Books Request'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="returned-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'book_id',
                'filter' => false,
                'format' => 'raw',
                'value' => function($searchModel) {
                    return Html::a($searchModel->book->isbn, Url::toRoute(['/book/view', 'id' => $searchModel->book_id]));
                },
            ],
            [
                'attribute' => 'user_id',
                'filter' => false,
                'format' => 'raw',
                'value' => function($searchModel) {
                    return Html::a($searchModel->user->email, 'mailto:' . $searchModel->user->email);
                },
            ],
            'borrowed_date:date',
            'due_date:date',
            'returned_date:date',
            [
                'attribute' => 'status',
                'filter' => false,
                'format' => 'raw',
                'value' => function($model) {
                    return '<span class="label label-default">' . $model->getStatusArray()[$model->status] . '</span>';
                }
            ]
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
