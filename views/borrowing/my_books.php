<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use Carbon\Carbon;
use kartik\icons\Icon;
Icon::map($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BorrowingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Personal Library');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pikups-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($searchModel) {
            if($searchModel->isOverdue()) {
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            [
                'attribute' => 'book_id',
                'filter' => false,
                'format' => 'raw',
                'value' => function($searchModel) {
                    return Html::a($searchModel->book->isbn, Url::toRoute(['/book/preview', 'id' => $searchModel->book_id]));
                },
            ],
            [
                'attribute' => 'book.title',
                'value' => function($searchModel) {
                    return $searchModel->book->title;
                },
            ],
            [
                'attribute' => 'borrowed_date',
                'format' => 'raw',
                'value' => function($searchModel) {
                    if(empty($searchModel->borrowed_date))
                        return '<em class="color-green">Go & Get It ' . Icon::show('arrow-right') . '</em>';
                    return Carbon::createFromFormat('Y-m-d H:i:s', $searchModel->borrowed_date)->diffForHumans();
                },
            ],
            [
                'attribute' => 'due_date',
                'value' => function($searchModel) {
                    if(!empty($searchModel->due_date))
                        return Carbon::createFromFormat('Y-m-d', $searchModel->due_date)->diffForHumans();
                },
            ],
            'returned_date:date',
            [
                'attribute' => 'status',
                'filter' => false,
                'format' => 'raw',
                'value' => function($model) {
                    if($model->status == \app\models\Borrowing::STATUS_OVERDUE)
                        return '<span class="label label-danger">' . $model->getStatusArray()[$model->status] . '</span>';
                    return '<span class="label label-default">' . $model->getStatusArray()[$model->status] . '</span>';
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>

