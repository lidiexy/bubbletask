<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Borrowing */

$this->title = Yii::t('app', 'New Book Loan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Books Request'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="borrowing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'books' => $books,
        'readers' => $readers
    ]) ?>

</div>
