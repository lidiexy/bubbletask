<?php
use yii\helpers\Html;
use yii\helpers\Url;
use Carbon\Carbon;
use kartik\icons\Icon;
Icon::map($this);

/* @var $model \app\models\Book */
?>

<div class="panel">
    <div class="panel-body">
        <h6 class="text-semibold no-margin-top">
            <?= Html::a(Html::encode($model->title), ['/book/preview', 'id' => $model->id]); ?>
        </h6>
        <h6 class="text-success pull-right no-margin text-semibold">$<?= money_format('%.2n', $model->price); ?></h6>
        <div class="book-author">
            <small class="text-muted">by <?= Html::encode($model->author); ?> </small>
        </div>

        <?= \yii\helpers\StringHelper::truncateWords($model->review, 11); ?>

        <!--<ul class="list-inline list-inline-condensed no-margin-bottom mt-15">
            <li>(<?/*=  $model->existence */?>) <small class="text-muted">in stock</small></li>
            <li><?/*= Html::a('Details &nbsp; ' . Icon::show('angle-right'), ['/book/view', 'id' => $model->id], ['class' => 'btn btn-small btn-default']); */?></li>
        </ul>-->
    </div>

    <div class="panel-footer panel-footer-condensed">
        <div class="heading-elements not-collapsible">
            <span class="heading-text">
                <small class="text-muted">Published on </small>
                <?= Carbon::createFromFormat('Y-m-d', $model->publication_date)->toFormattedDateString(); ?>
            </span>
            <span class="heading-text pull-right label label-warning">
                <?= $model->genre ?>
            </span>
        </div>
    </div>
</div>

