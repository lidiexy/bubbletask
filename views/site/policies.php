<?php
use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'Circulation Policies Library BubbleBooks';
$this->params['breadcrumbs'][] = Html::encode($this->title);

?>
<div class="site-policies">
    <div class="jumbotron">
        <h1 class="color-orange"><?= Yii::t('app', 'Circulation Policies'); ?></h1>
        <p class="lead">
            <?= Yii::t('app', 'Our staff have some rules to use this system and the Library.'); ?>
            <?= Yii::t('app', 'Please, read carefully all the information.') ?>
        </p>
    </div>
</div>

