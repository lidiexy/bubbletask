<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact Us';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
        <div class="alert alert-success">
            <?= Yii::t('app','Thank you for contacting us. We will respond to you as soon as possible.') ?>
        </div>
    <?php else: ?>
        <div class="jumbotron">
            <h1 class="color-orange"><?= Html::encode($this->title) ?></h1>
            <p class="lead">
                <?= Yii::t('app', 'If you have library inquiries or other questions, please fill out the following form to contact us.') ?>
                <?= Yii::t('app','Thank you.') ?>
            </p>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($model, 'email') ?>
                    <?= $form->field($model, 'subject') ?>
                    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>

                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('app','Send Information'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
