<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

?>
<div class="book-search-list">
    <?php $form = ActiveForm::begin([
        'formConfig'=>['showLabels'=>false],
        'action' => ['index'],
        'method' => 'get',
        'type' => ActiveForm::TYPE_VERTICAL
    ]); ?>

    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <?= $form->field($model, 'search_listing', [
            'addon' => [
                'groupOptions' => ['class'=>'input-group-lg'],
                'append' => ['content'=>'<i class="glyphicon glyphicon-search"></i>']
            ]
        ])->textInput(['placeholder'=>'Search by ISBN/Title/Author/Genre']) ?>
    </div>

    <?= Html::submitButton(Yii::t('app', 'Find Your Next Reading'), ['class' => 'btn btn-success btn-large']) ?>
    <?php ActiveForm::end(); ?>
</div>

