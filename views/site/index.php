<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */

$this->title = 'Library BubbleBooks';
?>
<div class="site-index">
    <?php if (Yii::$app->session->hasFlash('bookRequest')): ?>
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <?= Yii::t('app', 'Thank you for your interest in our materials. Please, check your email for more information.'); ?>
        </div>
    <?php endif; ?>

    <div class="jumbotron">
        <h1 class="color-orange"><?= Yii::t('app', 'Good Reader, Great Mind!'); ?></h1>

        <p class="lead"><?= Yii::t('app', 'We have {book_count} materials and counting.', ['book_count' => $book_count]); ?> </p>
        <?= $this->render('_search_books', ['model' => $searchModel]); ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="container">
                <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'options' => [
                        'tag' => 'div',
                        'class' => 'list-wrapper',
                        'id' => 'list-wrapper',
                    ],
                    'layout' => "{pager}\n{items}",
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('_list_item',['model' => $model]);
                    },
                ]);
                ?>
            </div>

        </div>

    </div>
</div>
