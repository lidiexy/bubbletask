<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About us';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <div class="jumbotron">
        <h1 class="color-orange">BubbleBook Public Library</h1>
        <p class="lead">
            This <strong>Public Library</strong> is project of &copy; <?= Html::a('BubbleUp', 'http://bubbleup.com'); ?>,
            proudly develop by <?= Html::a('Lidiexy Alonso', 'https://www.linkedin.com/in/lidiexy/'); ?>.
        </p>
    </div>
</div>
