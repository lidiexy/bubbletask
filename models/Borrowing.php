<?php

namespace app\models;

use Yii;
use app\models\Book;
use app\models\User;
use Carbon\Carbon;

/**
 * This is the model class for table "borrowing".
 *
 * @property integer $id
 * @property string $borrowed_date
 * @property string $due_date
 * @property string $returned_date
 * @property integer $book_id
 * @property integer $user_id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Book $book
 * @property User $user
 */
class Borrowing extends \yii\db\ActiveRecord
{
    const STATUS_ON_HOLD = 1;
    const STATUS_HELD = 2;
    const STATUS_RETURNED = 3;
    const STATUS_OVERDUE = 4;
    const MAX_LOAN_DAYS = 7;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'borrowing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['borrowed_date', 'due_date', 'returned_date', 'created_at', 'updated_at'], 'safe'],
            [['book_id', 'user_id', 'status'], 'required'],
            [['book_id', 'user_id', 'status'], 'integer'],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Book::className(), 'targetAttribute' => ['book_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'borrowed_date' => Yii::t('app', 'Borrowed Date'),
            'due_date' => Yii::t('app', 'Due Date'),
            'returned_date' => Yii::t('app', 'Returned Date'),
            'book_id' => Yii::t('app', 'What Book?'),
            'user_id' => Yii::t('app', 'What Reader?'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::className(), ['id' => 'book_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return BorrowingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BorrowingQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => function ($event) {
                    return gmdate("Y-m-d H:i:s");
                },
            ],
        ];
    }

    public function getStatusArray()
    {
        return [
            self::STATUS_ON_HOLD => 'On Hold',
            self::STATUS_HELD => 'Held',
            self::STATUS_RETURNED => 'Returned',
            self::STATUS_OVERDUE => 'Overdue',
        ];
    }

    /**
     * Check if the loan it's overdue unless the status remain unchanged
     * @return bool
     */
    public function isOverdue()
    {
        if($this->status == self::STATUS_OVERDUE) {
            return true;
        } else {
            if(empty($this->due_date))
                return false;
            $today = Carbon::today();
            $due = Carbon::createFromFormat('Y-m-d', $this->due_date);
            if($today > $due) {
                return true;
            }
        }
        return false;
    }
}
