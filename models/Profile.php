<?php

namespace app\models;

use Yii;
use amnah\yii2\user\models\Profile as BaseProfile;
use yii\helpers\ArrayHelper;

/**
 * Class Profile
 *
 * @package \app\models
 * @property string $phone
 * @property string $address
 */

class Profile extends BaseProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $parent_rules = parent::rules();
        $child_rules = [
            [['phone'], 'string', 'max' => 15],
            [['address'], 'string', 'max' => 255],
        ];
        return ArrayHelper::merge($parent_rules, $child_rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $parent_labels = parent::attributeLabels();
        $child_labels = [
            'phone' => Yii::t('user', 'Phone Number'),
            'address' => Yii::t('user', 'Address'),
        ];
        return ArrayHelper::merge($parent_labels, $child_labels);
    }

}
