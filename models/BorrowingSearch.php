<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Borrowing;

/**
 * BorrowingSearch represents the model behind the search form about `app\models\Borrowing`.
 */
class BorrowingSearch extends Borrowing
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'book_id', 'user_id', 'status'], 'integer'],
            [['borrowed_date', 'due_date', 'returned_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $restriction = null)
    {
        $query = Borrowing::find()->with('book')->with('user');
        //Apply restriction to the query defined on BorrowingQuery
        if($restriction)
            $query->$restriction();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'borrowed_date' => $this->borrowed_date,
            'due_date' => $this->due_date,
            'returned_date' => $this->returned_date,
            'book_id' => $this->book_id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
