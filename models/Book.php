<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\Borrowing;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $isbn
 * @property string $title
 * @property string $genre
 * @property string $author
 * @property string $publication_date
 * @property integer $existence
 * @property integer $total_inventory
 * @property string $price
 * @property string $review
 * @property string $created_at
 * @property string $updated_at
 */
class Book extends \yii\db\ActiveRecord
{
    const BOOK_GENRES = [
        'Adventures' => 'Adventures',
        'Comedy' => 'Comedy',
        'Novels' => 'Novels',
        'Sci-Fi' => 'Sci-Fi'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'genre', 'author', 'publication_date'], 'required'],
            [['publication_date', 'created_at', 'update_at'], 'safe'],
            [['existence', 'total_inventory'], 'integer'],
            [['price'], 'number'],
            [['review'], 'string'],
            [['isbn', 'title', 'genre', 'author'], 'string', 'max' => 255],
            [['isbn'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'isbn' => Yii::t('app', 'ISBN'),
            'title' => Yii::t('app', 'Title'),
            'genre' => Yii::t('app', 'Genre'),
            'author' => Yii::t('app', 'Author'),
            'publication_date' => Yii::t('app', 'Published'),
            'existence' => Yii::t('app', 'Existence'),
            'total_inventory' => Yii::t('app', 'Inventory'),
            'price' => Yii::t('app', 'Price'),
            'review' => Yii::t('app', 'Review')
        ];
    }

    /**
     * @inheritdoc
     * @return BookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => function ($event) {
                    return gmdate("Y-m-d H:i:s");
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($insert && $this->isNewRecord) {
            $this->existence = $this->total_inventory;
        }

        return parent::beforeSave($insert);
    }

    /** Relations */
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReaders()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable(Borrowing::tableName(), ['book_id' => 'id']);
    }

    /** Mutator & Helpers */
    /**
     *  Decrement the existence in Inventory of this Book
     */
    public function decrementExistence()
    {
        if($this->existence > 0) {
            $this->updateAttributes(['existence' => ($this->existence - 1)]);
        }
    }

    /**
     * Increments the existence in Inventory
     */
    public function increaseExistence()
    {
        if($this->existence < $this->total_inventory) {
            $this->updateAttributes(['existence' => ($this->existence + 1)]);
        }
    }

}
