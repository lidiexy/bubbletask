<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Borrowing]].
 *
 * @see Borrowing
 */
class BorrowingQuery extends \yii\db\ActiveQuery
{
    public function requested()
    {
        return $this->andWhere('[[status]]=' . Borrowing::STATUS_ON_HOLD);
    }

    public function held()
    {
        return $this->andWhere('[[status]]=' . Borrowing::STATUS_HELD);
    }

    public function overdue()
    {
        return $this->andWhere('[[status]]>=' . Borrowing::STATUS_OVERDUE)
            ->orWhere('[[due_date]]<CURDATE()');
    }

    public function ontime()
    {
        return $this->andWhere('[[status]]<' . Borrowing::STATUS_OVERDUE);
    }

    /**
     * @inheritdoc
     * @return Borrowing[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Borrowing|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
