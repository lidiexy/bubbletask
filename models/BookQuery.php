<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Book]].
 *
 * @see Book
 */
class BookQuery extends \yii\db\ActiveQuery
{
    public function available()
    {
        return $this->andWhere('[[existence]]>0');
    }

    public function drained()
    {
        return $this->andWhere('[[existence]]=0');
    }

    /**
     * @inheritdoc
     * @return Book[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Book|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
