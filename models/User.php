<?php

namespace app\models;

use amnah\yii2\user\models\User as BaseUser;
use app\models\Book;

/**
 * Upgrade User information.
 *
 * @property Book $books[]
 */

class User extends BaseUser
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Book::className(), ['id' => 'book_id'])
            ->viaTable(Borrowing::tableName(), ['user_id' => 'id']);
    }

    public function getBooksOverdue()
    {
        return $this->hasMany(Book::className(), ['id' => 'book_id'])
            ->viaTable(Borrowing::tableName(), ['user_id' => 'id'])
            ->onCondition(['status' => Borrowing::STATUS_OVERDUE]);
    }
}
