<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Book;
use yii\helpers\ArrayHelper;

/**
 * BookSearch represents the model behind the search form about `app\models\Book`.
 */
class BookSearch extends Book
{
    public $search_listing;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'existence', 'total_inventory'], 'integer'],
            [['isbn', 'title', 'genre', 'author', 'publication_date', 'review', 'search_listing'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'title' => SORT_DESC,
                ],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if(isset($this->search_listing) && !empty($this->search_listing)) {
            $query->orFilterHaving(['like', 'isbn', $this->search_listing])
                ->orFilterHaving(['like', 'title', $this->search_listing])
                ->orFilterHaving(['like', 'author', $this->search_listing])
                ->orFilterHaving(['like', 'genre', $this->search_listing]);

            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'publication_date' => $this->publication_date,
            'existence' => $this->existence,
            'total_inventory' => $this->total_inventory,
            'price' => $this->price,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'isbn', $this->isbn])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'genre', $this->genre])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'review', $this->review]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'search_listing' => Yii::t('app', 'Search Materials'),
        ]);
    }
}
