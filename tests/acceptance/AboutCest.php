<?php
use yii\helpers\Url as Url;

class AboutCest
{
    public function ensureThatAboutWorks(AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/site/about'));
        $I->see('BubbleBook Public Library', 'h1');
        $I->see('BubbleUp', 'a');
    }
}
