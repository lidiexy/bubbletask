<?php

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ContactForm;
use app\models\BookSearch;
use Faker\Factory;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'book_count' => $dataProvider->totalCount,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionGenerate()
    {
        if(!Yii::$app->user->isGuest && Yii::$app->user->can('admin')) {
            $start = microtime(true);
            $faker = Factory::create();
            $data = [];
            for($i=1; $i <= 10; $i++) {
                $existence = rand(1, 50);
                $data[$i] = [
                    $faker->isbn10,
                    $faker->name,
                    $faker->sentence(6),
                    $faker->randomElement(['Adventures','Comedy','Novels','Sci-Fi']),
                    $faker->dateTimeThisCentury->format('Y-m-d'),
                    $existence,
                    $existence,
                    $faker->randomFloat(2,0,100),
                    $faker->realText(100, 1),
                    $faker->dateTime->format('Y-m-d H:i:s')
                ];
            }
            Yii::$app->db->createCommand()->batchInsert('books', ['isbn', 'author', 'title', 'genre', 'publication_date', 'existence', 'total_inventory', 'price', 'review', 'created_at'], $data)->execute();

            $time_elapsed_us = microtime(true) - $start;
            
            $this->redirect(Yii::$app->homeUrl);
        } else {
            $this->redirect(Yii::$app->homeUrl);
        }
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays Circulation Policies page.
     *
     * @return string
     */
    public function actionCirculationPolicies()
    {
        return $this->render('policies');
    }
}
