<?php

namespace app\controllers;

use app\models\Book;
use app\models\Profile;
use app\models\User;
use Carbon\Carbon;
use Yii;
use app\models\Borrowing;
use app\models\BorrowingSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BorrowingController implements the CRUD actions for Borrowing model.
 */
class BorrowingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['my-books'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'delete',
                            'update',
                            'returned',
                            'return-book',
                            'pickup',
                            'pickups',
                            'overdue-lends',
                            'overdue-email',
                            'overdue-bulk'
                        ],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'pickup' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Borrowing models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BorrowingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Borrowing Ready for Pickup.
     * @return mixed
     */
    public function actionPickups()
    {
        $searchModel = new BorrowingSearch(['status' => Borrowing::STATUS_ON_HOLD]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('pickups', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Borrowing Ready for Pickup.
     * @return mixed
     */
    public function actionReturned()
    {
        $searchModel = new BorrowingSearch(['status' => Borrowing::STATUS_RETURNED]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('returned', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Borrowing Ready for Pickup.
     * @return mixed
     */
    public function actionOverdueLends()
    {
        $searchModel = new BorrowingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'overdue');

        return $this->render('overdue_lends', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Borrowing By the Current User.
     * @return mixed
     */
    public function actionMyBooks()
    {
        $searchModel = new BorrowingSearch(['user_id' => Yii::$app->user->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('my_books', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    /**
     * Displays a single Borrowing model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Borrowing model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Borrowing();
        $all_books = ArrayHelper::map(Book::find()->all(), 'id', 'title');
        $all_readers = ArrayHelper::map(User::find()->all(), 'id', 'displayName');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'books' => $all_books,
                'readers' => $all_readers
            ]);
        }
    }

    /**
     * Updates an existing Borrowing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $all_books = ArrayHelper::map(Book::find()->all(), 'id', 'title');
        $all_readers = ArrayHelper::map(User::find()->all(), 'id', 'displayName');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'books' => $all_books,
                'readers' => $all_readers
            ]);
        }
    }

    /**
     * Deletes an existing Borrowing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Pickup the book at the Library
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionPickup($id)
    {
        /** @var Borrowing $model */
        $model = $this->findModel($id);
        if($model->book->existence <= 0) {
            Yii::$app->session->setFlash('zeroExistence');
            return $this->redirect(['pickups']);
        }
        $now = Carbon::now();
        $model->borrowed_date = $now;
        $model->due_date = $now->copy()->addDays(Borrowing::MAX_LOAN_DAYS);
        $model->status = Borrowing::STATUS_HELD;
        $model->book->decrementExistence();
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Return this Book to the Inventory
     * @param $id
     * @return \yii\web\Response
     */
    public function actionReturnBook($id)
    {
        $model = $this->findModel($id);
        $now = Carbon::now();
        if(empty($model->borrowed_date)) {
            $model->borrowed_date = $now;
            $model->due_date = $now->copy()->addDays(Borrowing::MAX_LOAN_DAYS);
        }
        $model->returned_date = $now;
        $model->status = Borrowing::STATUS_RETURNED;
        $model->book->increaseExistence();
        if($model->save()) {
            Yii::$app->mailer->compose('book-returned')
                ->setTo($model->user->email)
                ->setFrom([Yii::$app->params['adminEmail'] => 'BubbleBook Librarian'])
                ->setSubject(Yii::t('app', 'BubbleBook material returns to the Library. Thanks'))
                ->send();

            Yii::$app->session->setFlash('bookRequest');
        }

        return $this->redirect(['index']);
    }

    /**
     * Send Individual Email after Overdue
     * @param $id
     * @return \yii\web\Response
     */
    public function actionOverdueEmail($id)
    {
        $model = $this->findModel($id);
        Yii::$app->mailer->compose('book-overdue')
            ->setTo($model->user->email)
            ->setFrom([Yii::$app->params['adminEmail'] => 'BubbleBook Librarian'])
            ->setSubject(Yii::t('app', 'Warning! Some fees will applied cause your overdue.'))
            ->send();

        Yii::$app->session->setFlash('bookOverdue');

        return $this->redirect(['overdue-lends']);
    }

    /**
     * Send Bulk Email to Overdues
     * @return \yii\web\Response
     */
    public function actionOverdueBulk()
    {
        $items = Borrowing::find()->with('book')->with('user')->overdue()->all();
        $emails = [];
        foreach ($items as $item) {
            /** @var Borrowing $item */
            $emails[$item->user->email] = $item->user->displayName;
        }
        Yii::$app->mailer->compose('book-overdue')
            ->setTo($emails)
            ->setFrom([Yii::$app->params['adminEmail'] => 'BubbleBook Librarian'])
            ->setSubject(Yii::t('app', 'Warning! Some fees will applied cause your overdue.'))
            ->send();

        Yii::$app->session->setFlash('bulkOverdue');

        return $this->redirect(['overdue-lends']);
    }

    /**
     * Finds the Borrowing model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Borrowing the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Borrowing::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
