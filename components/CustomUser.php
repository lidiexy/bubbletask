<?php

namespace app\components;

use amnah\yii2\user\components\User as BaseUserComponent;

/**
 * Class CustomUser
 *
 * @package \app\components
 */

class CustomUser extends BaseUserComponent
{
    /**
     * @inheritdoc
     */
    public $identityClass = 'app\models\User';

}
