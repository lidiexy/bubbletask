<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=bubbletask',
    'username' => 'homestead',
    'password' => 'secret',
    'charset' => 'utf8',
];
