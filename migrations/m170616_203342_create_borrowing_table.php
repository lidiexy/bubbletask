<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `borrowing`.
 */
class m170616_203342_create_borrowing_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('borrowing', [
            'id' => $this->primaryKey(),
            'borrowed_date' => Schema::TYPE_DATETIME,
            'due_date' => Schema::TYPE_DATE,
            'returned_date' => Schema::TYPE_DATETIME,
            'book_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status' => Schema::TYPE_SMALLINT . ' UNSIGNED NOT NULL',
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ]);

        $this->createIndex('idx-borrowing-user_id', 'borrowing', 'user_id');
        $this->addForeignKey(
            'fk-borrowing-user_id',
            'borrowing',
            'user_id',
            'user',
            'id'
        );

        $this->createIndex('idx-borrowing-book_id', 'borrowing', 'book_id');
        $this->addForeignKey(
            'fk-borrowing-book_id',
            'borrowing',
            'book_id',
            'books',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-borrowing-book_id', 'borrowing');
        $this->dropIndex('idx-borrowing-book_id', 'borrowing');

        $this->dropForeignKey('fk-borrowing-user_id', 'borrowing');
        $this->dropIndex('idx-borrowing-user_id', 'borrowing');

        $this->dropTable('borrowing');
    }
}
