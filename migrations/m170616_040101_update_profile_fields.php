<?php

use yii\db\Migration;
use yii\db\Schema;

class m170616_040101_update_profile_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('profile', 'address', Schema::TYPE_STRING);
        $this->addColumn('profile', 'phone', Schema::TYPE_STRING);

    }

    public function safeDown()
    {
        $this->dropColumn('profile', 'address');
        $this->dropColumn('profile', 'phone');
    }
}
