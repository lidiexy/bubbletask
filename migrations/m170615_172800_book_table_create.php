<?php

use yii\db\Migration;
use yii\db\Schema;

class m170615_172800_book_table_create extends Migration
{
    public function safeUp()
    {
        $this->createTable('books', [
            'id' => Schema::TYPE_PK,
            'isbn' => Schema::TYPE_STRING . ' UNIQUE',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'genre' => Schema::TYPE_STRING . ' NOT NULL',
            'author' => Schema::TYPE_STRING . ' NOT NULL',
            'publication_date' => Schema::TYPE_DATE . ' NOT NULL',
            'existence' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'price' => Schema::TYPE_MONEY,
            'review' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('books');
    }
}
