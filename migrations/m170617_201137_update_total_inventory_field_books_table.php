<?php

use yii\db\Migration;
use yii\db\Schema;

class m170617_201137_update_total_inventory_field_books_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('books', 'total_inventory', Schema::TYPE_INTEGER);

    }

    public function safeDown()
    {
        $this->dropColumn('books', 'total_inventory');
    }
}
