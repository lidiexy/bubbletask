<?php
/**
 * Translation map for es-ES
 * Just an example of how to do translations in this case to Spanish From default en-EN
 */
return [
    'Good Reader, Great Mind!' => '¡Buen Lector, Mente Grandiosa!',
    'Thank you for your interest in our materials. Please, check your email for more information.' => 'Gracias por su interes en nuestros materials. Por favor, revise su correo electronico para mas informacion.'
];
